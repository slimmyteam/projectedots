using System.Linq;
using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;

namespace fornicioygenocidio
{
    partial struct ReproductiveSystem : ISystem
    {
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<Reproduction>();
            state.RequireForUpdate<MachoTag>();
            state.RequireForUpdate<HembraTag>();
        }

        [BurstCompile]
        public void OnDestroy(ref SystemState state)
        {
        }

        public void OnUpdate(ref SystemState state)
        {
            //var machoEntitiesQuery = SystemAPI.QueryBuilder().WithAll<MachoAspect>().Build();

            //UnityEngine.Debug.Log("Entro en el update");
            //Aquest sistema far� canvis estructurals (afegir, treure, activar o crear components).
            //�s per aix� que necessitem una refer�ncia al commandBuffer.
            //En concret just despr�s de fer les simulacions, hi ha diversos segons la prefer�ncia.

            
            var reproduction = SystemAPI.GetSingletonRW<Reproduction>();

            var ecbSingleton = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            EntityCommandBuffer ecb = ecbSingleton.CreateCommandBuffer(state.WorldUnmanaged);

            float reproductiveRange = reproduction.ValueRO.detectionRadiusRange;
            reproductiveRange *= reproductiveRange;

            foreach (var hembra in SystemAPI.Query<HembraAspect>())
            {
                if (!hembra.Disponible(SystemAPI.Time.DeltaTime))
                    return;
                foreach (var macho in SystemAPI.Query<MachoAspect>())
                {
                    if (math.distancesq(hembra.Position, macho.Position) < reproductiveRange)
                    {
                        var random = reproduction.ValueRW.random.NextInt(0, 2);
                        Entity prefab;
                        Entity retono;
                        if (random == 0)
                            prefab = reproduction.ValueRO.hembraPrefab;
                        else
                            prefab = reproduction.ValueRO.machoPrefab;

                        retono = ecb.Instantiate(prefab);
                        ecb.SetComponent(retono, LocalTransform.FromPosition(hembra.Position));

                        float3 direccion = reproduction.ValueRW.random.NextFloat3(-1, 1);
                        direccion.y = 0;
                        Speed speed = SystemAPI.GetComponent<Speed>(prefab);
                        ecb.SetComponent(retono, new Speed
                        {
                            speed = speed.speed,
                            direction = direccion,
                            cooldown = speed.cooldown,
                            random =  new Unity.Mathematics.Random((ushort)UnityEngine.Random.Range(1, 65536))
                        });
                    }
                }
            }


            //if(cycles == 100) 
            //    state.Enabled = false;

            //ecb.Playback(state.EntityManager);

            //ProximityJob proximityJob = new ProximityJob
            //{
            //    ECB = ecb,
            //    inRangeEntities = ,
            //};

            //proximityJob.ScheduleParallel();
        }

    }

    //[BurstCompile]
    partial struct ProximityJob : IJobEntity
    {
        public EntityCommandBuffer ECB;
        public Unity.Collections.NativeArray<MachoAspect> inRangeEntities;

        //[BurstCompile]
        void Execute(HembraAspect hembra)
        {
            //if (math.distancesq(hembra.Position, macho.Position) < hembra.DistanciaApareamiento)
            //{
            //    Entity retono = state.EntityManager.Instantiate(hembra.HembraPrefab);
            //    state.EntityManager.SetComponentData(retono, LocalTransform.FromPosition(hembra.Position));
            //}
        }
    }
}
