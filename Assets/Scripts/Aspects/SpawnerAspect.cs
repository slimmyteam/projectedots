//using Unity.Collections;
//using Unity.Entities;
//using Unity.Mathematics;
//using Unity.Transforms;

//// L'aspecte ens servir� com a helper per al nostre
//// component Spawner.
//// D'aquesta forma hi tindrem depend�ncies, inclourem
//// tot all� que ens interessa i farem funcions per a
//// modificar els diferents components.
//readonly partial struct SpawnerAspect : IAspect
//{
//    //Aix� �s opcional, �s una refer�ncia a nosaltres
//    //si el poseu, autom�ticament queda referenciat.
//    //private readonly Entity entity;

//    //refer�ncia que farem al nostre component spawner
//    private readonly RefRW<Spawner> m_Spawner;

//    private Entity Prefab => m_Spawner.ValueRO.entityPrefab;

//    public void ElapseTime(float deltaTime, EntityManager em)
//    {
//        m_Spawner.ValueRW.elapsedTime -= deltaTime;
//        if(m_Spawner.ValueRO.elapsedTime <= 0)
//        {
//            m_Spawner.ValueRW.elapsedTime += m_Spawner.ValueRO.spawnRate;
//            Spawn(em);
//        }
//    }

//    private void Spawn(EntityManager em)
//    {
//        //Instanciar UNA
//        //Entity entity = em.Instantiate(Prefab);
//        //Instanciar M�LTIPLES
//        var entities = em.Instantiate(Prefab, m_Spawner.ValueRO.spawnAmount, Allocator.Temp);

//        foreach(Entity entity in entities)
//        {
//            //inicialitzem els components de la entitat spawnejada
//            float3 direction = math.normalize(m_Spawner.ValueRW.random.NextFloat3(-1, 1));
//            float3 position = m_Spawner.ValueRW.random.NextFloat3(-10, 10);
//            float speed = m_Spawner.ValueRW.random.NextFloat(.5f, 5f);
            
//            em.SetComponentData(entity, new Speed
//            {
//                speed = speed,
//                direction = direction
//            });
            
//            em.SetComponentData(entity, new LocalTransform
//            {
//                Position = position,
//                Scale = 1
//            });

//            //Per defecte les entitats no s�n a prop del jugador
//            //em.SetComponentEnabled<IsInRangeTag>(entity, false);
//        }
//    }
//}
