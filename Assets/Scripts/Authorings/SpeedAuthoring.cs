
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace fornicioygenocidio
{
    public class SpeedAuthoring : MonoBehaviour
    {
        public float speed;
        public Vector3 direction;
        public float cooldown;

        [Header("Random")]
        public bool useSeed = false;
        public ushort seed = 1;

        class SpeedBaker : Baker<SpeedAuthoring> //sirve para transformar el monobehaviour en una entity y le ponga los componentes
        {
            public override void Bake(SpeedAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);

                AddComponent(entity, new Speed
                {
                    speed = authoring.speed,
                    direction = authoring.direction.normalized,
                    cooldown = authoring.cooldown,
                    random = authoring.useSeed ?
                      new Unity.Mathematics.Random(authoring.seed)
                    : new Unity.Mathematics.Random((ushort)UnityEngine.Random.Range(0, 65536))
                });
            }
        }
    }

    public struct Speed : IComponentData //las entities solo pueden tener datos y por eso hacemos un struct de las variables (data no admite vectors)
    {
        public float speed;
        public float3 direction;
        public float cooldown;
        //Per a fer random, cal desar l'estat del random en dades
        public Unity.Mathematics.Random random;
    }

}