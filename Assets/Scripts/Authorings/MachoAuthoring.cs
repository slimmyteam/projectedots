using Unity.Entities;
using UnityEngine;

namespace fornicioygenocidio
{
    public class MachoAuthoring : MonoBehaviour
    {

        class MachoAuthoringBaker : Baker<MachoAuthoring>
        {
            public override void Bake(MachoAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);
                AddComponent(entity, new MachoTag { }); ;
            }
        }
    }

    public struct MachoTag : IComponentData
    {
    }
}
