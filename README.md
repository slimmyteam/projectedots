# _DOTS (ECS Unity System)_
A través de una simulación en que una entidad "femenina" y otra "masculina" se reproducen al entrar en contacto, se trabaja el uso del _Entity Component System_ (ECS) de Unity para la gestión de una grandísima cantidad de objetos en ejecución con un bajo consumo de recursos por parte del PC. Además, se incluye característica para poder eliminar en área una gran cantidad de estos objetos.

Para conocer más sobre DOTS, sigue el siguiente enlace: [DOTS (ECS)](https://unity.com/es/dots).

## 📄 Descripción
Proyecto realizado para la asignatura de "Videojuegos" desarrollado durante el segundo año de Grado Superior de Desarrollo de Aplicaciones Multiplataforma + Perfil videojuegos y ocio digital (DAMvi).

## 💻 Tecnologías
![C#](https://img.shields.io/badge/c%23-%23239120.svg?style=for-the-badge&logo=csharp&logoColor=white)
![Unity](https://img.shields.io/badge/unity-%23000000.svg?style=for-the-badge&logo=unity&logoColor=white)

## ⚙️ Funcionamiento (clic en la imagen para ver el vídeo)
El funcionamiento se puede leer en el siguiente documento:
[Explicación DOTS](https://gitlab.com/slimmyteam/unity/projectedots/-/blob/main/DocumentDots.pdf?ref_type=heads).

[![](http://img.youtube.com/vi/QL_MMsYyqMg/0.jpg)](http://www.youtube.com/watch?v=QL_MMsYyqMg "DOTS - ECS")


## ©️ Desarrolladores
- Ethan Corchero Martín.
- Ana María Gómez León.
- Selene Milanés Rodríguez.

🎶 Música: _Reflections_ por ParallaxProductions.
