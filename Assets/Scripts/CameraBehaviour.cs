using UnityEngine;

namespace fornicioygenocidio
{
    public class CameraBehaviour : MonoBehaviour
    {

        [SerializeField]
        private float m_MovementSpeed = 5f;
        [SerializeField]
        private GameObject bombaPF;

        public Unity.Mathematics.float3 Position { get; internal set; }
        public static CameraBehaviour Instance { get; internal set; }

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else
                Destroy(gameObject);

            Position = transform.position;
        }


        private void Update()
        {
            Vector3 movement = Vector3.zero;
            if (Input.GetKey(KeyCode.W))
                movement += Vector3.forward;
            if (Input.GetKey(KeyCode.S))
                movement -= Vector3.forward;
            if (Input.GetKey(KeyCode.A))
                movement -= Vector3.right;
            if (Input.GetKey(KeyCode.D))
                movement += Vector3.right;
            if (Input.GetKey(KeyCode.Q))
                movement += Vector3.up;
            if (Input.GetKey(KeyCode.E))
                movement -= Vector3.up;
            transform.position += movement.normalized * m_MovementSpeed * Time.deltaTime;
            Position = transform.position;
        }
    }
}
