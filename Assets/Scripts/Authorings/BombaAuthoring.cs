using Unity.Entities;
using UnityEngine;

namespace fornicioygenocidio
{
    public class BombaAuthoring : MonoBehaviour
    {
        public float explosionRadius;
        public GameObject bombaPrefab;

        class BombaBaker : Baker<BombaAuthoring>
        {
            public override void Bake(BombaAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);
                AddComponent(entity, new Kaboom
                {
                    explosionRadius = authoring.explosionRadius,
                    bombaPrefab = GetEntity(authoring.bombaPrefab, TransformUsageFlags.Dynamic),
                });
            }
        }
    }

    public struct Kaboom : IComponentData
    {
        public float explosionRadius;
        public Entity bombaPrefab;
    }
}
