using Unity.Entities;
using UnityEngine;

namespace fornicioygenocidio
{
    public class ReproductiveAuthoring : MonoBehaviour
    {
        public float detectionRadiusRange;
        public GameObject hembraPrefab;
        public GameObject machoPrefab;

        [Header("Random")]
        public bool useSeed = false;
        public ushort seed = 1;

        class ReproductiveBaker : Baker<ReproductiveAuthoring>
        {
            public override void Bake(ReproductiveAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.None);
                AddComponent(entity, new Reproduction
                {
                    detectionRadiusRange = authoring.detectionRadiusRange,
                    hembraPrefab = GetEntity(authoring.hembraPrefab, TransformUsageFlags.Dynamic),
                    machoPrefab = GetEntity(authoring.machoPrefab, TransformUsageFlags.Dynamic),
                    random = authoring.useSeed ?
                      new Unity.Mathematics.Random(authoring.seed)
                    : new Unity.Mathematics.Random((ushort)UnityEngine.Random.Range(1, 65536))
                });
            }
        }
    }

    public struct Reproduction : IComponentData
    {
        public float detectionRadiusRange;
        public Entity hembraPrefab;
        public Entity machoPrefab;
       
        //Per a fer random, cal desar l'estat del random en dades
        public Unity.Mathematics.Random random;
    }
}
