using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Transforms;
using UnityEngine;

namespace fornicioygenocidio
{
    partial struct BombaSystem : ISystem
    {
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<Kaboom>();
        }

        [BurstCompile]
        public void OnDestroy(ref SystemState state)
        {
        }

        public void OnUpdate(ref SystemState state)
        {
            var kaboom = SystemAPI.GetSingletonRW<Kaboom>();
            var camera = CameraBehaviour.Instance;

            var ecbSingleton = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            EntityCommandBuffer ecb = ecbSingleton.CreateCommandBuffer(state.WorldUnmanaged);

            if (Input.GetKeyDown(KeyCode.Space))
            {
                Entity bombaPrefab = kaboom.ValueRO.bombaPrefab;
                //Debug.Log("bombaPrefab: " + bombaPrefab);
                Entity finalBomb = ecb.Instantiate(bombaPrefab);
                ecb.SetComponent(finalBomb, LocalTransform.FromPosition(new float3(camera.Position.x, camera.Position.y, camera.Position.z+20)));
            }

            var physicsWorld = SystemAPI.GetSingleton<PhysicsWorldSingleton>().PhysicsWorld;

            foreach (var bomba in SystemAPI.Query<BombaAspect>())
            {
                if (!bomba.MustExplode())
                    return;
                float explosionRange = kaboom.ValueRO.explosionRadius;
                NativeList<ColliderCastHit> colliderCastHits = new NativeList<ColliderCastHit>(Allocator.Temp);
                if (physicsWorld.SphereCastAll(bomba.Position, explosionRange, float3.zero, explosionRange, ref colliderCastHits, CollisionFilter.Default))
                {
                    foreach (ColliderCastHit hit in colliderCastHits)
                    {
                        ecb.DestroyEntity(hit.Entity);
                    }
                }
                ecb.DestroyEntity(bomba.Entity);
            }
        }
    }
}
