using Unity.Entities;
using UnityEngine;

namespace fornicioygenocidio
{
    public class HembraAuthoring : MonoBehaviour
    {
        public float cooldown;
        class HembraAuthoringBaker : Baker<HembraAuthoring>
        {
            public override void Bake(HembraAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);
                AddComponent(entity, new HembraTag
                {
                    cooldown = authoring.cooldown,
                }); 
            }
        }
    }

    public struct HembraTag : IComponentData
    {
        public float cooldown;
    }


}
