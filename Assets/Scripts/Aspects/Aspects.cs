using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace fornicioygenocidio
{
    readonly partial struct MachoAspect : IAspect
    {
        private readonly RefRO<LocalTransform> m_Transform;
        private readonly RefRO<MachoTag> m_Tag;

        public float3 Position => m_Transform.ValueRO.Position;
    }

    readonly partial struct HembraAspect : IAspect
    {
        private readonly RefRO<LocalTransform> m_Transform;
        private readonly RefRW<HembraTag> m_Tag;

        public float3 Position => m_Transform.ValueRO.Position;
        public float Cooldown => m_Tag.ValueRW.cooldown;

        public bool Disponible(float deltaTime)
        {
            m_Tag.ValueRW.cooldown -= deltaTime;
            if (m_Tag.ValueRO.cooldown <= 0)
            {
                m_Tag.ValueRW.cooldown += 1;
                return true;
            }
            return false;
        }
    }

    readonly partial struct SpeedAspect : IAspect
    {
        private readonly Entity entity;
        private readonly RefRW<Speed> m_Speed;

        public float3 Direccion => m_Speed.ValueRW.direction;
        private readonly RefRO<LocalTransform> m_Transform;
        public float3 Position => m_Transform.ValueRO.Position;
        public Entity Entity => entity;

        public void RandomizarDireccion(float deltaTime)
        {
            m_Speed.ValueRW.cooldown -= deltaTime;

            if (m_Speed.ValueRW.cooldown > 0)
                return;

            m_Speed.ValueRW.cooldown += 15;
            m_Speed.ValueRW.direction = m_Speed.ValueRW.random.NextFloat3(-1f, 1f);
            m_Speed.ValueRW.direction.y = 0;
        }
    }

    readonly partial struct BombaAspect : IAspect
    {
        private readonly Entity entity;
        private readonly RefRO<BombaTag> m_Tag;
        private readonly RefRO<LocalTransform> m_Transform;
        public float3 Position => m_Transform.ValueRO.Position;

        public Entity Entity => entity;
        public bool MustExplode()
        {
            if (m_Transform.ValueRO.Position.y <= 0)
                return true;
            return false;
        }
    }
}
