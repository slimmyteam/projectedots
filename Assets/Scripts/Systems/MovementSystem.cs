using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace fornicioygenocidio
{
    partial struct MovementSystem : ISystem
    {
        //En el system lo que hacemos es la l�gica de la entidad
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            //Si no hi ha cap entity amb un Speed component, no s'executa el sistema
            state.RequireForUpdate<Speed>();
        }

        [BurstCompile]
        public void OnDestroy(ref SystemState state)
        {
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {

            float deltaTime = SystemAPI.Time.DeltaTime;

            MovementJob movement = new MovementJob
            {
                deltaTime = deltaTime
            };

            foreach (var person in SystemAPI.Query<SpeedAspect>())
            {
                person.RandomizarDireccion(deltaTime);
            }
                //Podem paral�lelitzar
                movement.ScheduleParallel();

            //Equivale al job pero no lo hara en un hilo en paralelo

            //foreach (var (transform, speed) in SystemAPI.Query<RefRW<LocalTransform>, RefRO<Speed>>())
            //{
            //    float3 direction = speed.ValueRO.direction;
            //    transform.ValueRW.Position += new float3(direction.x, direction.y, direction.z) * speed.ValueRO.speed * deltaTime;
            //}
        }
    }
    [BurstCompile]
    partial struct MovementJob : IJobEntity //equivale a hacer un hilo
    {
        public float deltaTime;

        //Com a par�metres de l'execute vindriem a posar les condicions de cerca del nostre sistema:
        //in -> read-only
        //ref -> read-write
        [BurstCompile]
        void Execute(ref LocalTransform transform, in Speed speed)
        {
            float3 direction = speed.direction;
            transform.Position += new float3(direction.x, 0, direction.z) * speed.speed * deltaTime;
        }
    }


}