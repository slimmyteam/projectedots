using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

namespace fornicioygenocidio
{
    public class BombaTagAuthoring : MonoBehaviour
    {
        class BombaTagAuthoringBaker : Baker<BombaTagAuthoring>
        {
            public override void Bake(BombaTagAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);
                AddComponent(entity, new BombaTag { }); ;
            }
        }
    }

    public struct BombaTag : IComponentData
    {
    }
}
